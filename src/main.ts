import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.enableCors();
  await app.listen(8080);
}
bootstrap();
