export class CartItemDto {
   item: CartDto[]
}

export class CartDto {
    id: number;
    name: string;
    quantity: number;
    price: number;
}
