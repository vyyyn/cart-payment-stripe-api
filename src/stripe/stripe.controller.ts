import { Body, Controller, HttpException, HttpStatus, Post } from '@nestjs/common';
import { CartItemDto } from './cart.dto';
import { StripeService } from './stripe.service';

@Controller('/stripe')
export class StripeController {
    constructor(private stripeService: StripeService) { }

    @Post()
    checkout(@Body() cartItemDto: CartItemDto) {
        try {
            return this.stripeService.checkout(cartItemDto?.item)
        } catch (e) {
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: e?.sqlMessage,
            }, HttpStatus.FORBIDDEN, {
                cause: e
            });
        }
    }
}
