import { Injectable } from '@nestjs/common';
import Stripe from 'stripe';
import { CatItemType } from './stripe.type';
@Injectable()
export class StripeService {
    private stripe;
    constructor() {
        this.stripe = new Stripe("sk_test_51Om78zGRyjBIjx96wGFQ8dTTzH3yElrnoODIiYfUNNyen32zJPawStOkx4XlZWxfTt74e4OEkQw3GhVQfFSB3EW600T26kHhKz", {
            apiVersion: '2023-10-16'
        })
    }

    async checkout(cart: CatItemType[]) {
        const totalItem = await cart.reduce((total, item) => total + item.quantity * item.price, 0)
        return await this.stripe.paymentIntents.create({
            amount: totalItem * 100,
            currency: 'usd',
            payment_method_types: ['card']
        })
    }
}
