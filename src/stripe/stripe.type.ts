export type CatItemType = {
    id: number,
    name: string,
    quantity: number,
    price: number
}